package com.example.simpleeventreceiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


public class CustomAshwini extends BroadcastReceiver
{
	private static final String TAG = "MainActivity:CustomAshwini";
    private NotificationManager mNotificationManager;
	public static final int NOTIFICATION_ID = 1;
	
	public CustomAshwini()
	{
	
	}
	@Override
	public void onReceive(Context context, Intent intent)
	{
		 if (intent.getAction().equals(Intent.ACTION_TIMEZONE_CHANGED)) {
	        	Log.d(TAG,"Time Zone Manually Changed!");
	        	sendNotification(context,"Time Zone Manually Changed!");
	        }
		// TODO Auto-generated method stub
		 //Toast.makeText(context, "Time Zone Changed", Toast.LENGTH_LONG).show();
	}
	private void sendNotification(Context context, String msg) {
        mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
    
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
            new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder = 
        	new NotificationCompat.Builder(context)
        		.setSmallIcon(R.drawable.ic_launcher)
        		.setContentTitle(context.getString(R.string.hello_world))
        		.setStyle(new NotificationCompat.BigTextStyle()
        		.bigText(msg))
        		.setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}