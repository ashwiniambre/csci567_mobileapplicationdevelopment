package com.example.userinterface;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity 
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final TextView textView1=(TextView)findViewById(R.id.textView1);
		final Button button1=(Button) findViewById(R.id.button1);
		button1.setOnClickListener(new View.OnClickListener(){ 
            public void onClick(View v) 
            {
        		textView1.setText("You have clicked BUTTON 1");
            }
		});
		final Button button2=(Button) findViewById(R.id.button2);
		button2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) 
            {
        		textView1.setText("You have clicked BUTTON 2");
            }
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
