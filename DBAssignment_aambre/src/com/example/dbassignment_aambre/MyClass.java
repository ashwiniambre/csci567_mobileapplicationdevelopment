package com.example.dbassignment_aambre;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class MyClass extends SQLiteOpenHelper
{	
	
	public static final String DatabaseName = "Databasename.db";
	public static final int DatabaseVersion = 1;
	private static final String TableName = "MyTable_DB";
	Context context;
	
	public MyClass(Context context)
	{
		super(context,DatabaseName , null, DatabaseVersion);
		this.context=context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TableName + "(text VARCHAR primary key);");
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{	
		db.execSQL("DROP TABLE IF EXISTS " + TableName); 
        onCreate(db);
		// TODO Auto-generated method stub
	}
	
	public boolean insertText(String text)
	{
		try
		{
			SQLiteDatabase sqldb=this.getWritableDatabase();
		Log.d("DB Insert: ", "INSERT OR REPLACE INTO " +
    			TableName + " (text) Values ("+ text + ");");
		sqldb.execSQL("INSERT OR REPLACE INTO " +
    			TableName + " (text) Values (\""+ text + "\");");
		sqldb.close();
		}
		catch(SQLiteException e)
		{
			Log.d("DB Insert Error", e.toString());
			return false;
		}
		return true;
	}
	
	public String getText()
	{
		String toReturn = " ";
		try
		{
			SQLiteDatabase qdb = this.getReadableDatabase();
			qdb.execSQL("CREATE TABLE IF NOT EXISTS " + TableName + " (text VARCHAR Primary Key);");
			Cursor c = qdb.rawQuery("SELECT * FROM " +TableName, null);
			if (c != null ) 
			{
	    		if  (c.moveToFirst()) 
	    		{
	    			do 
	    			{
	    				String text = c.getString(c.getColumnIndex("text"));
	    				toReturn += text + "\n";
	    			}
	    			while (c.moveToNext());
	    		}
			}
			qdb.close(); 
		}
		catch(SQLiteException se)
		{
			Log.d("DB Select Error: ",se.toString());
			return "";
		}
		return toReturn;
	}


}
