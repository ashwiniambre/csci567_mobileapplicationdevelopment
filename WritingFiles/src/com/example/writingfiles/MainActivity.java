package com.example.writingfiles;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Bundle;
import android.os.Environment;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener 
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(this);
		Button button2 = (Button) findViewById(R.id.button2);
		button2.setOnClickListener(this);
		Button button3 = (Button) findViewById(R.id.button3);
		button3.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private boolean writeFile(String str)
	{
		try
		{	//File f=new File(Environment.get);
			File f=new File(Environment.getExternalStorageDirectory().getPath()+"/test.txt");
			Log.d("File Write", Environment.getExternalStorageDirectory().getPath()+"/test.txt");
			if(!f.exists())
			{
				f.createNewFile();
			}
			FileWriter fw=new FileWriter(f.getAbsoluteFile());
			BufferedWriter bw=new BufferedWriter(fw);
			bw.write(str);
			bw.close();
			
			Log.d("FileWrite", "Successful!!");
		}
		catch(IOException ioe)
		{
			Log.d("Error Writing into wile", ioe.toString());
		}
		return false;
	
	}
	
	private boolean appendFile(String str)
	{
		try
		{	//File f=new File(Environment.get);
			File f=new File(Environment.getExternalStorageDirectory().getPath()+"/test.txt");
			Log.d("File Write", Environment.getExternalStorageDirectory().getPath()+"/test.txt");
			if(!f.exists())
			{
				f.createNewFile();
			}
			str = "\n" + str;
			FileWriter fw=new FileWriter(f.getAbsoluteFile(),true);
			BufferedWriter bw=new BufferedWriter(fw);
			bw.write(str);
			bw.close();
			
			Log.d("FileWrite", "Successful!!");
		}
		catch(IOException ioe)
		{
			Log.d("Error Writing into wile", ioe.toString());
		}
		return false;
	
	}
	
	private String readFile()
	{
		Log.d("FileRead:", "Starting");
		String toReturn="";
		String str = "";
		try
		{
			File f=new File(Environment.getExternalStorageDirectory().getPath()+"/test.txt");
			if(!f.exists())
			{
				Log.d("FileRead:", "No File");
				return "";
			}
			FileReader fr=new FileReader(f.getAbsoluteFile());
			BufferedReader br= new BufferedReader(fr);
			while((str=br.readLine())!=null)
			{
				toReturn = toReturn + str + "\n";
			}
			br.close();
			Log.d("FileRead: ", "Successful");
		}
		catch(IOException e)
		{
			Log.d("FilereaderError: ", e.toString());
		}
		return toReturn;
	}
	
	@SuppressLint("CutPasteId")
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.button1:
			EditText t1=(EditText) findViewById(R.id.edittext);
			writeFile(t1.getText().toString());
			break;
		case R.id.button2:
			//Log.d("read","In switch case");
			TextView t2=(TextView) findViewById(R.id.textView1);
			t2.setText(readFile());
			break;
		case R.id.button3:
			//Log.d("read","In switch case");
			EditText t3=(EditText) findViewById(R.id.edittext);
			appendFile(t3.getText().toString());
			break;
			
		}
		// TODO Auto-generated method stub
	}
}
